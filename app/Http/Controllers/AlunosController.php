<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AlunoCreateRequest;
use App\Http\Requests\AlunoUpdateRequest;
use App\Repositories\AlunoRepository;
use App\Services\AlunoService;

/**
 * Class AlunosController.
 *
 * @package namespace App\Http\Controllers;
 */
class AlunosController extends Controller
{
    /**
     * @var AlunoRepository
     */
    protected $repository;

    /**
     * @var AlunoValidator
     */
    protected $validator;

    /**
     * AlunosController constructor.
     *
     * @param AlunoRepository $repository
     * @param AlunoValidator $validator
     */
    public function __construct(AlunoRepository $repository, AlunoService $service)
    {
        $this->repository = $repository;
        $this->service    = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $alunos = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $alunos,
            ]);
        }

        return view('alunos.index', compact('alunos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AlunoCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AlunoCreateRequest $request)
    {
        $request = $this->service->store($request->all());
        $aluno   = $request['success'] ? $request['data'] : null;

        session()->flash('success', [
            'success'   => $request['success'],
            'errors'  => $request['message']
        ]);

        return redirect()->route('alunos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aluno = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $aluno,
            ]);
        }

        return view('alunos.show', compact('aluno'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aluno = $this->repository->find($id);

        return view('alunos.edit', compact('aluno'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AlunoUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AlunoUpdateRequest $request, $id)
    {
        $request = $this->service->update($request->all(), $id);
        $aluno   = $request['success'] ? $request['data'] : null;

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['message']
        ]);

        return redirect()->route('alunos');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Aluno deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Aluno deleted.');
    }
}
