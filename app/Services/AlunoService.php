<?php

namespace App\Services;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Validators\AlunoValidator;
use App\Repositories\AlunoRepository;

/**
 * Class AlunoService
 * @author Hiago Couto
 */
class AlunoService
{
    /**
     * @var AlunoRepository
     */
    protected $repository;

    /**
     * @var AlunoValidator
     */
    protected $validator;


    public function __construct(AlunoRepository $repository, AlunoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  array $data
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            $aluno = $this->repository->create($data);

            return [
                'success'    => true,
                'data'      => $aluno,
                'message'   => 'Aluno Cadastrado'
            ];
            
        } catch (\Exception $ex) {

            switch (get_class($ex)) {
                case 'Illuminate\Database\QueryException' : return ['success' => false, 'error' => $ex->getMessage(), 'message' => 'O Cadastro Falhou'];
                case 'Prettus\Validator\Exceptions\ValidatorException' : return ['success' => false, 'message' => $ex->getMessageBag()];
                default                                   : return ['success' => false, 'error' => $ex->getMessage(), 'code' => '400'];
            }
        }
    }

    /**
     * Update a created resource.
     *
     * @param  array $data
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(array $data, $id)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $aluno = $this->repository->update($data, $id);

            return [
                'success'    => true,
                'data'      => $aluno,
                'message'   => 'Aluno Atualizado'
            ];
            
        } catch (\Exception $ex) {

            switch (get_class($ex)) {
                case 'Illuminate\Database\QueryException' : return ['success' => false, 'message' => $ex->getMessage()];
                case 'Prettus\Validator\Exceptions\ValidatorException' : return ['success' => false, 'message' => $ex->getMessageBag()];
                default                                   : return ['success' => false, 'error' => $ex->getMessage(), 'code' => '400'];
            }
        }
    }

}
