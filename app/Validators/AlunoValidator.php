<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AlunoValidator.
 *
 * @package namespace App\Validators;
 */
class AlunoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'nome'       => 'required|max:40|alpha_spaces',
            'nmatricula' => 'required|min:11|max:15|string|regex:/^[0-9]+$/',
            'status'     => 'required|alpha|in:AT,IT',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'nome'       => 'required|max:40|alpha_spaces',
            'nmatricula' => 'required|min:11|max:15|string|regex:/^[0-9]+$/',
            'status'     => 'required|alpha|in:AT,IT',
        ],
    ];
}
