<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Aluno;
use Faker\Generator as Faker;

$factory->define(Aluno::class, function (Faker $faker) {
    return [
        'nome'           => $faker->name(40), 
        'nmatricula'     => $faker->numerify('###############'), 
        'status'         => $faker->randomElement(array('AT', 'IT')),
    ];
});
