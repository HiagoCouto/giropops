@extends('templates.master')

@section('conteudo-view')
{!! Form::model($aluno, ['route' => ['alunos.update', $aluno->id], 'method' => 'put', 'class' => 'form-padrao']) !!}
	@include('templates.formulario.input', ['label' => "Nome do Aluno", 'input' => 'nome', 'attributes' => ['placeholder' => "Nome do Aluno"]])
    @include('templates.formulario.input', ['label' => "Matricula", 'input' => 'nmatricula', 'attributes' => ['placeholder' => "Matricula"]])
    @include('templates.formulario.select', ['label' => "Status", 'select' => 'status', 'data' => array('AT' => 'Ativo', 'IT' => 'Inativo'), 'attributes' => ['placeholder' => "Tipo"]])
	@include('templates.formulario.submit', ['input' => 'Atualizar'])
{!! Form::close() !!}
@endsection
