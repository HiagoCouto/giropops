@extends('templates.master')

@section('css-view')
@endsection

@section('js-view')
@endsection

@section('conteudo-view')
    {!! Form::open(['route' => 'alunos.store', 'method' => 'post', 'class' => 'form-padrao']) !!}
        <div class="form-row">
            @include('templates.formulario.input', ['label' => "Nome do Aluno", 'input' => 'nome', 'attributes' => ['placeholder' => "Nome do Aluno", 'class' => 'form-control']])
            @include('templates.formulario.input', ['label' => "Matricula", 'input' => 'nmatricula', 'attributes' => ['placeholder' => "Matricula", 'class' => 'form-control']])
        </div>

        <div class="form-row">
            @include('templates.formulario.select', ['label' => "Status", 'select' => 'status', 'data' => array('AT' => 'Ativo', 'IT' => 'Inativo'), 'attributes' => ['placeholder' => "Tipo", 'class' => 'form-control']])
            @include('templates.formulario.submit', ['input' => 'Cadastrar'], ['class' => 'btn btn-success'])
        </div>
    {!! Form::close() !!}

@include('alunos.list', ['alunos_list' => $alunos])
@endsection
