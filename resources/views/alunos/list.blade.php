
<div class="row">
    <div class="col-md-12">
        @if(session('success')['success'])
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
            {{session('success')['errors']}}
        </div>
        @endif

        @if(!session('success')['success'] && isset(session('success')['success']))
        <div class="alert alert-danger alert-dismissible flat no-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Whoops!</strong><br>
            <ul>
                @foreach (session('success')['errors']->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped table-index table-index-td">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Matrícula</th>
                <th>Status</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
		    @foreach($alunos_list as $aluno)
            <tr>
                <td class="h6">{{ $aluno->id }}</td>
                <td class="h6">{{ $aluno->nome }}</td>
                <td class="h6">{{ $aluno->nmatricula }}</td>
                @if($aluno->status == 'AT')
                    <td class="h6">Ativo</td>
                @else
                    <td class="h6">Inativo</td>
                @endif
                <td>
                    <a href="{{ route('alunos.show', $aluno->id) }}" title="Ver Aluno"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                    <a href="{{ route('alunos.edit', $aluno->id) }}" title="Editar Aluno"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></button></a>

                    <!--
				{!! Form::open(['method' => 'DELETE', 'route' => ['alunos.destroy', $aluno->id]]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', ['type' => 'submit', 'class'=>'btn btn-danger btn-sm']) !!}
				{!! Form::close() !!}
                -->

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
