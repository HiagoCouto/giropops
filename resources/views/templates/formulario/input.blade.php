@php
	$attributes['placeholder'] = $attributes['placeholder'] ?? $label;
@endphp
<!-- <span>{{ $label ?? $input ?? "ERRO" }}</span> -->
<div class="form-group col">
	{!! Form::text($input, $value ?? null, $attributes) !!}
</div>
