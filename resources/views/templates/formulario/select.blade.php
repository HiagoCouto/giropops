<div class="form-group col">
    {!! Form::select($select, $data ?? [], $value ?? null, $attributes) !!}
</div>
