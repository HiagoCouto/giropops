<?php

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;
use Faker\Factory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Entities\Aluno;

class AlunosTestController extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function can_create_a_aluno()
    {
        $this->withoutExceptionHandling();
        $faker = Factory::create();
            
        $post = [
            'nome'       => $nome       = $faker->name(40), 
            'nmatricula' => $nmatricula = $faker->numerify('###############'), 
            'status'     => $status     = $faker->randomElement(array('AT', 'IT'))
        ];

        $respost = [
            'success'  => true,
            'data'    => [
                'nome'       => $nome, 
                'nmatricula' => $nmatricula, 
                'status'     => $status
            ],
            'message' => 'Aluno Cadastrado'
        ];

        $response = $this->post('/alunos', $post);

        $this->assertDatabaseHas('alunos', $post);
    }

    /**
     */
    public function will_fail_with_errors_when_try_create_a_aluno()
    {
        $this->withoutExceptionHandling();
        $faker = Factory::create();
            
        $post = [
            'nome'       => 'E1 Invalifo 23', 
            'nmatricula' => '1221EEEE3', 
            'status'     => $faker->randomLetter(),
        ];

        $respost = [
            'success'  => false,
            'error'   => [
                "nome" => [
                    0 => "The nome may only contain letters and spaces."
                ],
                "nmatricula" => [
                    0 => "The nmatricula must be at least 11 characters.",
                    1 => "The nmatricula format is invalid."
                ],
                "status" => [
                    0 => "The selected status is invalid."
                ]
            ],
            'message' => 'O Cadastro Falhou'
        ];
        $response = $this->post('/alunos', $post);

        $this->assertEquals($respost, json_decode($response->getContent(), true));
    }

    /**
     * @test
     */
    public function can_show_a_aluno()
    {
        $this->withoutExceptionHandling();
        $aluno = $this->create('Aluno');

        $request = $this->get("/alunos/$aluno->id");

        $request->assertOk();
        $this->assertEquals([
                'id'             => $aluno->id,
                'nome'           => $aluno->nome, 
                'nmatricula'     => $aluno->nmatricula, 
                'status'         => $aluno->status,
                'created_at'     => $aluno->created_at,
                'updated_at'     => $aluno->updated_at
            ], json_decode($request->getContent(), true));
    }
}
